"use strict";
exports.__esModule = true;
var PerfectPDF = /** @class */ (function () {
    function PerfectPDF(document) {
        this.document = document;
    }
    PerfectPDF.prototype.process = function (definition) {
        var content = Array.isArray(definition.content) ? definition.content : [definition.content];
        for (var _i = 0, content_1 = content; _i < content_1.length; _i++) {
            var def = content_1[_i];
            def.process(this.document);
        }
    };
    PerfectPDF.prototype.getDocument = function () {
        return this.document;
    };
    return PerfectPDF;
}());
exports["default"] = PerfectPDF;
