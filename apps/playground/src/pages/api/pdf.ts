import { NextApiRequest, NextApiResponse } from "next";

import PDFDocument from 'pdfkit';
import PerfectPDF from '@perfect-pdf/document';


const lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in suscipit purus. Vestibulum ante ipsum primis in ' +
  'faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus nec hendrerit felis. Morbi aliquam facilisis risus eu ' +
  'lacinia. Sed eu leo in turpis fringilla hendrerit. Ut nec accumsan nisl. Suspendisse rhoncus nisl posuere tortor ' +
  'tempus et dapibus elit porta. Cras leo neque, elementum a rhoncus ut, vestibulum non nibh. Phasellus pretium justo ' +
  'turpis. Etiam vulputate, odio vitae tincidunt ultricies, eros odio dapibus nisi, ut tincidunt lacus arcu eu elit. Aenean ' +
  'velit erat, vehicula eget lacinia ut, dignissim non tellus. Aliquam nec lacus mi, sed vestibulum nunc. Suspendisse ' +
  'potenti. Curabitur vitae sem turpis. Vestibulum sed neque eget dolor dapibus porttitor at sit amet sem. Fusce a turpis ' +
  'lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae';




const handler = async (req: NextApiRequest, res: NextApiResponse) => {

  res.setHeader('Content-Type', 'application/pdf');
  res.setHeader('Content-Disposition', 'inline; filename=dummy.pdf');
  //await pipeline(response.body, res);



  var doc = new PDFDocument();
  var stream = doc.pipe(res);

  doc.fillColor('red')
    .text(lorem.slice(0, 199), {
      width: 465,
      height: 100,
      columns: 3,
      continued: true
    })
    .fillColor('blue')
    .text(lorem.slice(199, 282), doc.x - 50, doc.y - 10, {
      link: 'http://www.example.com',
      continued: true
    })
    .fillColor('green')
    .text(lorem.slice(182, 400), doc.x + 50, doc.y + 10, {
      link: null,  // ignore warning, nothing to see!
      ellipsis: true,
    });
  ;

  doc.fillColor('black').text(lorem);

  // end and display the document in the iframe to the right
  doc.end();

};

export default handler;