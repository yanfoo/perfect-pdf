/// <reference types="pdfkit" />
export interface Content {
    process(doc: PDFKit.PDFDocument): boolean | undefined;
}
export interface DocumentDefinition {
    content: Content | Content[];
}
//# sourceMappingURL=types.d.ts.map