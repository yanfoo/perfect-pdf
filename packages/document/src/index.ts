import type { DocumentDefinition, TPerfectPDF } from '@perfect-pdf/types';


export default class PerfectPDF implements TPerfectPDF {

   constructor(private document:PDFKit.PDFDocument) {
   }

   process(definition:DocumentDefinition):void {
      const content = Array.isArray(definition.content) ? definition.content : [definition.content];
      for (const def of content) {
         def(this.document);
      }
   }

   getDocument():PDFKit.PDFDocument {
      return this.document;
   }

}

