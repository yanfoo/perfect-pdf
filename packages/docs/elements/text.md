# Text Element

## Usage

```ts
const def = {
   content: [
      'Simple line 1',
      {
         text: 'Simple line 2'
      }
   ]
}
```

## Type

```ts
type Text = {
   text:string
   position:'absolute'|'relative'|undefined
   x:number|string|undefined
   y:number|string|undefined
   width:number|string|undefined
   height:number|string|undefined
   align:'left'|'right'|'center'|'justify'|undefined
   
}
```