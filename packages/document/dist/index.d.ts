/// <reference types="pdfkit" />
import type { DocumentDefinition, TPerfectPDF } from '@perfect-pdf/types';
export default class PerfectPDF implements TPerfectPDF {
    private document;
    private static document;
    constructor(document: PDFKit.PDFDocument);
    process(definition: DocumentDefinition): void;
    getDocument(): PDFKit.PDFDocument;
}
//# sourceMappingURL=index.d.ts.map