import type {
   THorizontalAlignment,
   TVerticalAlignment,
   TPosition,
   TPositionValue,
   TSizeValue,
   Content
} from '../utils';



export type TextOptions = {
   /** The text to display */
   text:string
   /** Whether the text position should be relative or absolute (defaults to relative) */
   position?:TPosition
   /** The X position to set or adjust the text */
   x?:TPositionValue
   /** The Y position to set or adjust the text */
   y?:TPositionValue
   /** The width of the text area, will default to the remaining width on page */
   width?:TSizeValue
   /** The height of the text area, will default to the remaining height on page */
   height?:TSizeValue
   /** The horizontal alignment of the text */
   align?:THorizontalAlignment
   /** The vertical alignment of the text */
   verticalAlign?:TVerticalAlignment
   /** The number of columns to flow the text into */
   columns?: number | undefined
   /** The amount of space between each column (1/4 inch by default) */
   columnGap?: number | undefined
   /** The amount in PDF points (72 per inch) to indent each paragraph of text */
   indent?: number | undefined
   /** The amount of space between each paragraph of text */
   paragraphGap?: number | undefined
   /** The amount of space between each line of text */
   lineGap?: number | undefined
   /** The amount of space between each word in the text */
   wordSpacing?: number | undefined
   /** The amount of space between each character in the text */
   characterSpacing?: number | undefined
   /** The font name for the text */
   font?: string | undefined
   /** The font size in PDF points (72 per inch) */
   fontSize?: number | undefined
   /** Whether to fill the text (true by default) */
   fill?: boolean | undefined
   /** Whether to stroke the text */
   stroke?: boolean | undefined
   /** A URL to link this text to (shortcut to create an annotation) */
   link?: string | undefined
   /** Whether the text segment will be followed immediately by another segment. Useful for changing styling in the middle of a paragraph. */
   continued?: boolean | undefined
   /** Whether to underline the text */
   underline?: boolean | undefined
   /** Whether to strike out the text */
   strike?: boolean | undefined
   /** Whether to slant the text (angle in degrees or true) */
   oblique?: boolean | number | undefined
}


interface ITextContent extends Content {
   (options:TextOptions):(doc:PDFKit.PDFDocument) => boolean | undefined;
}


export const TextContent:ITextContent = (options:TextOptions) => (doc:PDFKit.PDFDocument) => {

   return true;
};
