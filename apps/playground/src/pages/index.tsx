import { useEffect, useRef } from 'react';

import {EditorView, basicSetup} from "codemirror";
import {Compartment} from "@codemirror/state"
import {javascript} from "@codemirror/lang-javascript"


const language = new Compartment, tabSize = new Compartment

const fixedHeightEditor = EditorView.theme({
  "&": {height: "100%"},
  ".cm-scroller": {overflow: "auto"}
})



const DEFAULT_CODE = `{
  meta: {
    title: "Test PDF",
    author: "John Smith",
    subject: "Hello world example",
    keywords: "hello world test",
  },
  content: [
    {
      page: 'LETTER',
      orientation: 'PORTRAIT',
      margin: [20, 20, 20, 20]
    },
    "Header text",
    {
      text: '
    }

  ]
}`;





export default function Docs() {
  const editorRef = useRef<any>();

  useEffect(() => {
    const editor = new EditorView({
      doc: DEFAULT_CODE,
      extensions: [
        basicSetup,
        fixedHeightEditor,
        language.of(javascript()),
      ],
      parent: editorRef.current
    });

    return () => editor.destroy();
  }, []);

  return (
    <div style={{ height:'100%' }}>
      <h1>Docs</h1>
      <div style={{ display:'flex', flexDirection:'row', gap:'20px' }}>
        <div style={{ flex:1, display:'flex', height:'100%', flexDirection:'column' }}>
          <div ref={ editorRef } style={{ flex:1 }} ></div>
        </div>
        <div style={{ flex:1 }}>
          Col 2
        </div>
      </div>
    </div>
  );
}
