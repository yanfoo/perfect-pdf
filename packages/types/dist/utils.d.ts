export declare type TPosition = 'absolute' | 'relative' | undefined;
export declare type TPositionValue = number | undefined;
export declare type TSizeValue = number | undefined;
export declare type THorizontalAlignment = 'left' | 'right' | 'center' | 'justify' | string | undefined;
export declare type TVerticalAlignment = 'svg-middle' | 'middle' | 'svg-central' | 'bottom' | 'ideographic' | 'alphabetic' | 'mathematical' | 'hanging' | 'top' | number | undefined;
//# sourceMappingURL=utils.d.ts.map