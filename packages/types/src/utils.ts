
declare module PerfectPDF {}

export interface Content {
   ():(doc:PDFKit.PDFDocument) => boolean | undefined;
};

export interface DocumentDefinition {
   content: Content | Content[]
};


export interface TPerfectPDF {
   process(definition:DocumentDefinition):void
   getDocument():PDFKit.PDFDocument
}

// export interface TPerfectPDFExtension {
//    create(inst:TPerfectPDF):void
//    process(inst:TPerfectPDF):boolean|undefined
// }

export type TPosition = 'absolute' | 'relative' | undefined
export type TPositionValue = number | undefined
export type TSizeValue = number | undefined
export type THorizontalAlignment = 'left' | 'right' | 'center' | 'justify' | string | undefined
export type TVerticalAlignment = 'svg-middle' | 'middle' | 'svg-central' | 'bottom' | 'ideographic' | 'alphabetic' | 'mathematical' | 'hanging' | 'top' | number | undefined;