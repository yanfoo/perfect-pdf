/// <reference types="pdfkit" />
export interface Content {
    process(doc: PDFKit.PDFDocument): boolean | undefined;
}
export interface DocumentDefinition {
    content: Content | Content[];
}
export interface TPerfectPDF {
    process(definition: DocumentDefinition): void;
    getDocument(): PDFKit.PDFDocument;
}
export interface TPerfectPDFExtension {
    create(inst: TPerfectPDF): void;
    process(inst: TPerfectPDF): boolean | undefined;
}
//# sourceMappingURL=index.d.ts.map